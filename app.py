from unittest import result
from flask import Flask , jsonify , request
import DatosControle

app = Flask(__name__)

@app.route('/ping')
def ping():
    return jsonify({"message":"owo"})

@app.route("/Datos")
def getDatos():
    Clientes = DatosControle.ConsultarDatos()
    return jsonify(Clientes)

@app.route("/Datos/<id>")
def getDato(id):
    Cliente = DatosControle.ConsultarDato(id)
    return jsonify(Cliente)

@app.route("/Datos", methods=['POST'])
def AgregarDato():
    Cliente_Detalles = request.get_json() #vendraen formato json y tendremos un diccionario de la peticion
    Cliente=Cliente_Detalles["Cliente"]
    Notaria=Cliente_Detalles["Notaria"]
    Presupuesto=Cliente_Detalles["Presupuesto"]
    Gastos=Cliente_Detalles["Gastos"]
    Documento=Cliente_Detalles["Documento"]
    result=DatosControle.InsertarDato(Cliente,Notaria,Presupuesto,Gastos,Documento)
    return jsonify(result)

@app.route("/Datos" , methods=['PUT'])
def EditarDato():
    Cliente_Detalles=request.get_json()
    Id = Cliente_Detalles["Id"]
    Cliente=Cliente_Detalles["Cliente"]
    Notaria=Cliente_Detalles["Notaria"]
    Presupuesto=Cliente_Detalles["Presupuesto"]
    Gastos=Cliente_Detalles["Gastos"]
    Documento=Cliente_Detalles["Documento"]
    result=DatosControle.ActualizarDato(Id,Cliente,Notaria,Presupuesto,Gastos,Documento )
    return jsonify(result)

@app.route("/Datos/<id>" , methods=['DELETE'])
def EliminarDato(id):
    result = DatosControle.EliminarDato(id)
    return jsonify(result)

if __name__ == '__main__':
    app.run(debug=False, port=4000) 