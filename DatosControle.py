
from Final import get_db

#Recordar la api se comuncia con json y devuelve json.

def ConsultarDatos():
    db=get_db()
    cursor=db.cursor()
    cursor.execute("SELECT * FROM Clientes")
    return cursor.fetchall() #traer todos

def InsertarDato(Cliente,Notaria,Presupuesto,Gastos,Documento):
    db=get_db()
    cursor=db.cursor()
    Sentencia="INSERT INTO Clientes(Cliente, Notaria, Presupuesto, Gastos, Documento) VALUES (?, ?, ?, ?, ?)"
    cursor.execute(Sentencia,[Cliente, Notaria, Presupuesto, Gastos, Documento])
    db.commit()
    return True 

def ActualizarDato(Id,Cliente,Notaria,Presupuesto,Gastos,Documento):
    db=get_db()
    cursor=db.cursor()
    Sentencia="UPDATE Clientes SET Cliente = ?, Notaria = ?, Presupuesto = ?, Gastos = ?, Documento = ? WHERE Id = ?"
    cursor.execute(Sentencia, [Cliente, Notaria, Presupuesto, Gastos, Documento, Id])
    db.commit()
    return True

def EliminarDato(Id):
    db=get_db()
    cursor=db.cursor()
    Sentencia="DELETE FROM Clientes WHERE Id = ?"
    cursor.execute(Sentencia,[Id])
    db.commit() 
    return True

def ConsultarDato(Id):
    db=get_db()
    cursor=db.cursor()
    Sentencia="SELECT id,Cliente,Notaria,Presupuesto,Gastos,Documento FROM Clientes WHERE id = ?"
    cursor.execute(Sentencia,[Id])
    return cursor.fetchone() #traer solo uno 



