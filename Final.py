import sqlite3

DATABASE_AGENCIA="procesos.db"

def get_db():
    conn=sqlite3.connect(DATABASE_AGENCIA)
    c=conn.cursor()
    c.execute("""CREATE TABLE IF NOT EXISTS Clientes(
        Id  INTEGER PRIMARY KEY AUTOINCREMENT,
        Cliente TEXT NOT NULL,
        Notaria REAL NOT NULL,
        Presupuesto REAL NOT NULL,
        Gastos REAL NOT NULL,
        Documento TEXT NOT NULL)""")
    return conn







